#!/bin/sh

set -e

if automake-1.11 --version &> /dev/null; then
  automake_suffix='-1.11'
else
  automake_suffix=''
fi

mkdir -p m4 aux
aclocal${automake_suffix} ${ACLOCAL_FLAGS}
automake${automake_suffix} --add-missing --foreign
autoconf
intltoolize --force --copy --automake

CFLAGS=${CFLAGS=-ggdb}
LDFLAGS=${LDFLAGS=-Wl,-O1}
export CFLAGS LDFLAGS

if test -z "$NOCONFIGURE"; then
  ./configure "$@"
fi
