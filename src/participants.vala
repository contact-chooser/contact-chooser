/*
 * Copyright © 2011 Tiffany Antopolski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * licence, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Tiffany Antopolski <tiffany@antopolski.com>
 */

using Gtk;
using Folks;

public class Participants : Grid
{
  ContactSelector? selector;
  ListStore? listmodel;
  TreeSelection selection;

  public Participants ()
    {
       var scrolled_window = new ScrolledWindow (null, null);
       scrolled_window.set_policy (PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);
       scrolled_window.expand = true;

       var toolbar = new Toolbar ();
       var add = new ToolButton.from_stock (Stock.ADD);
       add.clicked.connect (add_sharers_cb);

       var remove = new ToolButton.from_stock (Stock.REMOVE);
       remove.clicked.connect (remove_sharers_cb);

       toolbar.insert (remove, 0);
       toolbar.insert (add, 0);

       var view = new TreeView ();
       view.headers_visible = false;
       setup_treeview (view);

       scrolled_window.add (view);

      attach (scrolled_window, 0, 0, 1, 1);
      attach (toolbar, 0, 1, 1, 1);
    }

  private void setup_treeview (TreeView view)
    {
      listmodel = new ListStore (3, typeof (Folks.Individual), typeof (Gdk.Pixbuf), typeof(string));
      listmodel.set_sort_column_id (2, SortType.ASCENDING);
      view.set_model (listmodel);
      var crp = new CellRendererPixbuf ();

      /* test */
      //view.insert_column_with_attributes (-1, "Id", new CellRendererText//(), "text", 0);
      view.insert_column_with_attributes (-1, _("Avatar"), crp, "pixbuf", 1);

      /* Translators: This is a column heading for a column of   * people's names: a noun. */
      view.insert_column_with_attributes (-1, _("Contact"), new  CellRendererText (), "text", 2);

      /* TODO: removing multiple selections is significantly more
       * involved, so we only support single selection mode for now.
       */
      selection = view.get_selection ();
    }

  private void set_avatar_for_row (TreeIter row, LoadableIcon? avatar)
    {    Gdk.Pixbuf? gpb = null;

      if (avatar != null)
         {
           try
             {
               gpb = new Gdk.Pixbuf.from_stream_at_scale (avatar.load (32,	null),  32, 32, true);
             }
           catch (Error e)
             {
               /* Don't render* the avatar */
               gpb = null;
               warning ("Error creating pixbuf from avatar: %s", e.message);
             }
         }

       listmodel.set (row, 1, gpb);
    }

  void selected_cb (Folks.Individual person)
    {
      TreeIter row;

      // Check if the person is already in the list
      if (listmodel.get_iter_first (out row))
        {
          do
            {
              Folks.Individual tmp;

              listmodel.get (row, 0, out tmp);

              if (tmp == person)
                {
                  // already added...
                  return;
                }
            }
          while (listmodel.iter_next (ref row));
        }

      listmodel.append (out row);
      listmodel.set (row, 0, person);
      listmodel.set (row, 1, person.avatar);
      listmodel.set (row, 2, person.alias);

      var avatar = person.avatar;

      if (avatar != null)
        {
          this.set_avatar_for_row (row, avatar);
        }
    }

  private void add_sharers_cb ()
    {
      if (selector == null)
        {
          selector = new ContactSelector ();
          selector.selected.connect (selected_cb);

          selector.prepare.begin ((obj, res) =>
                 {
                   selector.prepare.end (res);
                 });
        }

      selector.present ();
    }

  private void remove_sharers_cb ()
    {
      TreeIter row;

      if (selection.get_selected (null, out row))
        {
          listmodel.remove (row);
        }
    }

  static void window_destroy_cb (Widget window)
    {
        Gtk.main_quit ();
    }

  public static int main (string[] args)
    {
      init (ref args);

      Intl.bindtextdomain (BuildConf.GETTEXT_PACKAGE, BuildConf.LOCALE_DIR);
      Intl.textdomain (BuildConf.GETTEXT_PACKAGE);

//    var sample = new ContactSelector ();
      var sample = new Participants ();
      var window = new Window (WindowType.TOPLEVEL);
      window.title = _("Currently 'sharing' report with:");
      window.set_border_width (20);
      window.set_default_size (250, 500);

      window.add (sample);

      window.destroy.connect (window_destroy_cb);
   /* sample.prepare.begin ((obj, res) =>
        {
          sample.prepare.end (res);
        });
   */
      window.show_all ();
      Gtk.main ();
      return 0;
    }
}

// vim:sw=2 et
