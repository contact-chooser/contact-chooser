/*
 * Copyright © 2011 Tiffany Antopolski
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * licence, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Tiffany Antopolski <tiffany@antopolski.com>
 */

using Gtk;
using Folks;

public class ContactSelector : Dialog
{
  ListStore listmodel;
  TreeSelection selection;
  IndividualAggregator aggregator;

  public ContactSelector ()
    {
      title = _("Invite participants to ‘meeting’:");

      /* Translators: This is a button label: a verb. */
      add_buttons (Stock.CLOSE, ResponseType.CLOSE, Stock.CANCEL, ResponseType.CANCEL, _("Share"), ResponseType.OK);
      set_border_width (2);
      set_default_size (350, 300);

      var search = new Entry ();
      search.set_icon_from_stock (EntryIconPosition.SECONDARY, Stock.FIND);
      search.placeholder_text = _("Search or enter an email address");

      var scrolled_window = new ScrolledWindow (null, null);
      scrolled_window.set_policy (PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);
      scrolled_window.expand = true;

      var view = new TreeView ();
      view.headers_visible = false;
      view.set_search_column (2);
      view.set_search_entry (search);
      view.enable_search = true;

      scrolled_window.add (view);

      var permissions = new ComboBoxText ();
      permissions.append_text (_("Can Edit"));
      permissions.append_text (_("View Only"));
      permissions.active = 0;

      var email_notify = new CheckButton.with_mnemonic (_("_Notify new sharers by email"));
      email_notify.margin = 10;

      var grid = new Grid ();

      grid.attach (search, 0, 0, 2, 1);
      grid.attach (scrolled_window, 0, 1, 2,1);
      grid.attach (permissions, 0, 2, 1, 1);
      grid.attach (email_notify, 1, 2, 1, 1);

      (get_content_area () as Container).add (grid);
      setup_treeview (view);
      grid.show_all ();

      aggregator = new Folks.IndividualAggregator ();
      aggregator.individuals_changed.connect (ind_changed);

      foreach (var person in aggregator.individuals.entries)
        {
          add_individual (person.value);
        }
    }

  public async void prepare ()
    {
      try
        {
          yield this.aggregator.prepare ();
        }
      catch (Error e)
        {
          /* Display an error */
          var dialogue = new MessageDialog (this, DialogFlags.MODAL | DialogFlags.DESTROY_WITH_PARENT, MessageType.ERROR, ButtonsType.OK,
              _("Error Preparing Contact List"));
          dialogue.secondary_text = e.message;

          dialogue.run ();
        }
    }

  private void notify_avatar_cb (Object obj, ParamSpec pspec)
    {
      var individual = (Individual) obj;
      TreeIter row;

      // The individual's avatar has been updated, so update it in the tree view.
      if (listmodel.get_iter_first (out row))
        {
          do
            {
              Individual tmp;

              listmodel.get (row, 0, out tmp);

              if (individual == tmp)
                {
                  /* Set the avatar */
                  this.set_avatar_for_row (row, individual.avatar);
                  return;
                }
            } while (listmodel.iter_next (ref row));
        }

      assert_not_reached ();
    }

  private void set_avatar_for_row (TreeIter row, LoadableIcon? avatar)
    {
      Gdk.Pixbuf? gpb = null;

      if (avatar != null)
        {
          try
            {
              gpb = new Gdk.Pixbuf.from_stream_at_scale (avatar.load (32, null), 32, 32, true);
            }
          catch (Error e)
            {
              /* Don't render the avatar */
              gpb = null;
              warning ("Error creating pixbuf from avatar: %s", e.message);
            }
        }

      listmodel.set (row, 1, gpb);
    }

  private void add_individual (Folks.Individual person)
    {
      TreeIter row;

      listmodel.append (out row);
      listmodel.set (row, 0, person);
      listmodel.set (row, 2, person.alias);

      person.notify["avatar"].connect (this.notify_avatar_cb);

      var avatar = person.avatar;
      if (avatar != null)
        {
          this.set_avatar_for_row (row, avatar);
        }
    }

  private void remove_individual (Folks.Individual person)
    {
      TreeIter row;

      if (listmodel.get_iter_first (out row))
        {
          do
            {
              Folks.Individual tmp;

              listmodel.get (row, 0, out tmp);
              if (tmp == person)
                {
                  listmodel.remove (row);

                  person.notify["avatar"].disconnect (this.notify_avatar_cb);

                  return;
                }
            } while (listmodel.iter_next (ref row));
        }
      assert_not_reached ();
    }

  private void ind_changed (Folks.IndividualAggregator ind,
      Gee.Set<Folks.Individual> added,
      Gee.Set<Folks.Individual> removed,
      string? message,
      Folks.Persona? actor,
      Folks.GroupDetails.ChangeReason reason)
    {
      foreach (var person in removed)
        {
          remove_individual (person);
        }

      foreach (var person in added)
        {
          add_individual (person);
        }
    }

  private void setup_treeview (TreeView view)
    {
      listmodel = new ListStore (3, typeof (Folks.Individual), typeof (Gdk.Pixbuf), typeof (string));
      listmodel.set_sort_column_id (2, SortType.ASCENDING);
      view.set_model (listmodel);
      var crp = new CellRendererPixbuf ();

      /* test */
      //view.insert_column_with_attributes (-1, "Id", new CellRendererText (), "text", 0);

      view.insert_column_with_attributes (-1, _("Avatar"), crp, "pixbuf", 1);

      /* Translators: This is a column heading for a column of people's names: a noun. */
      view.insert_column_with_attributes (-1, _("Contact"), new CellRendererText (), "text", 2);

      selection = view.get_selection ();
      selection.set_mode (SelectionMode.MULTIPLE);
    }

  public signal void selected (Folks.Individual person);

  private void invite (TreeModel model, TreePath path, TreeIter row)
    {
      Folks.Individual sharer;
      model.get (row, 0, out sharer);

      selected (sharer);
    }

  protected override void response (int id)
    {
      switch (id)
        {
          case ResponseType.OK:
            selection.selected_foreach (invite);
            break;
          default:
            hide ();
            break;
        }
    }
}

// vim:sw=2 et
