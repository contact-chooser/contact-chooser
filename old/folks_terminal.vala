//file: vello.vala

//To compile: valac --pkg folks --pkg gtk+-3.0 vello.vala

class MyApplication : Object {
	Folks.IndividualAggregator aggregator;

	private void
	ind_changed (Folks.IndividualAggregator      ind,
		     Gee.Set<Folks.Individual>       added,
		     Gee.Set<Folks.Individual>       removed,
		     string?                         message,
		     Folks.Persona?                  actor,
		     Folks.GroupDetails.ChangeReason reason) {
		foreach (var person in added) {
			print ("%s\n", person.alias);
		}
	}

	private void it_is_done (Object? source, AsyncResult result) {

		print ("start with %d\n", aggregator.individuals.size);
	}


	private MyApplication () {
		aggregator = new Folks.IndividualAggregator ();
		aggregator.individuals_changed.connect (ind_changed);
		aggregator.prepare.begin (it_is_done);
	}

	~MyApplication () {
		print("final\n");
	}

	static int main (string[] args) {
		Gtk.init (ref args);
		var app = new MyApplication ();
		Gtk.main ();
		return 0;
	}
}
